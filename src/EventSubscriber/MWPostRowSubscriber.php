<?php

namespace Drupal\migrate_wizard\EventSubscriber;

use Drupal\cas\Service\CasUserManager;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigratePreRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This class is executed after row is saved.
 */
class MWPostRowSubscriber implements EventSubscriberInterface {

  /**
   * The CAS user manager Service.
   *
   * @var \Drupal\cas\Service\CasUserManager|null
   */
  protected $casUserManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MWPostRowSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\cas\Service\CasUserManager|null $cas_user_manager
   *   The CAS user manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection, ?CasUserManager $cas_user_manager = NULL) {
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->casUserManager = $cas_user_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[MigrateEvents::POST_ROW_SAVE][] = ['setCasUserName', 0];
    $events[MigrateEvents::PRE_ROW_SAVE][] = ['checkDuplicateAlias', 0];

    return $events;
  }

  /**
   * Set the CAS user name to the user entity after migrate.
   */
  public function setCasUserName(MigratePostRowSaveEvent $event) {
    if ($this->casUserManager !== NULL) {
      $source_config = $event->getMigration()->getSourceConfiguration();

      if (isset($source_config['bundle']) && $source_config['bundle'] === 'user' && isset($source_config['import_cas']) && $source_config['import_cas']) {
        $row = $event->getRow();
        $cas_name = $row->getSourceProperty('cas_name');

        if (!empty($cas_name)) {
          $destination_uids = $event->getDestinationIdValues();
          $new_uid = end($destination_uids);
          $account = $this->entityTypeManager
            ->getStorage('user')
            ->load($new_uid);
          $this->casUserManager->setCasUsernameForAccount($account, $cas_name);
        }
      }
    }
  }

  /**
   * Returns a SELECT query for the path_alias base table.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   A Select query object.
   */
  protected function getBaseQuery() {
    $query = $this->connection->delete('path_alias');
    $query->condition('path_alias.status', 1);
    return $query;
  }

  /**
   * Check if the alias source exists and call to deleteDuplicatedAlias.
   */
  public function checkDuplicateAlias(MigratePreRowSaveEvent $event) {
    $row = $event->getRow();
    if ($row->getSourceProperty('alias') !== NULL) {
      $this->deleteDuplicatedAlias($row->getSourceProperty('alias'), $row->getSourceProperty('language'));
    }
  }

  /**
   * Delete duplicated alias on destiny.
   */
  private function deleteDuplicatedAlias($alias, $langcode) {
    // See the queries above. Use LIKE for case-insensitive matching.
    $query = $this->getBaseQuery()
      ->condition('path_alias.alias', $this->connection->escapeLike($alias), 'LIKE');
    $query->condition('path_alias.langcode', $langcode, 'IN');
    $query->execute();
  }

}
