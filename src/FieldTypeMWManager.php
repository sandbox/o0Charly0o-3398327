<?php

namespace Drupal\migrate_wizard;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manager class for the platform plugins.
 */
class FieldTypeMWManager extends DefaultPluginManager {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The migration object.
   *
   * @var \Drupal\migrate\Plugin\MigrationInterface
   */
  protected $migration;

  /**
   * Constructs an SocialLinkFieldPlatformManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    EntityFieldManagerInterface $entity_field_manager,
    ConfigFactoryInterface $configFactory
  ) {
    parent::__construct('Plugin/FieldTypeMW', $namespaces, $module_handler, NULL, 'Drupal\migrate_wizard\Annotation\FieldType');
    $this->entityFieldManager = $entity_field_manager;
    $this->config = $configFactory->getEditable('migrate_wizard.settings');
    $this->alterInfo('migrate_wizard_info');
    $this->setCacheBackend($cache_backend, 'migrate_wizard_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createReflectionClass($plugin_id) {
    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass($plugin_id, $plugin_definition);

    return new \ReflectionClass($plugin_class);
  }

}
