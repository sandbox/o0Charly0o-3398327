<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OriginMediaFields.
 *
 * Provides form create and edit migration medias.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class OriginMediaFields extends BaseFormFields {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_bundle = NULL) {
    $this->originBundle = $origin_bundle;
    $this->originType = 'media';
    $origin_media_entity = $this->entityTypeManager->getStorage('origin_media')->load($this->originBundle);
    $this->currentConfig = $origin_media_entity->shared_configuration;
    $this->manageFormService->setCurrentConfig($origin_media_entity->shared_configuration);
    $this->fieldsOriginBundle = $this->manageFormService->getFieldsOriginBundle($origin_media_entity, $this->originType);
    $this->manageFormService->setFieldsOriginBundle($this->fieldsOriginBundle);
    $type_fields_checkboxes = [
      'import_path_alias' => $this->t('Import path alias'),
    ];
    $this->checkboxes = array_merge($this->checkboxes, $type_fields_checkboxes);
    $form['origin_type'] = [
      '#type' => 'hidden',
      '#value' => 'media',
    ];

    $origin_database_value = $origin_media_entity->database;
    $form['origin_database'] = [
      '#type' => 'hidden',
      '#value' => $origin_database_value,
    ];

    $this->langCodes['default_lang']['value'] = $this->mwManageDataService->getOriginDefaultLangcode();

    $config_translation = $this->mwManageDataService->getOriginLanguages($form['origin_type']['#value'], $origin_media_entity->id_origin);

    foreach ($config_translation['origin_languages'] as $language) {
      $this->langCodes[$language . '_lang'] = [
        'markup' => $language,
        'value' => $language,
      ];
    }

    if (\count($config_translation['origin_languages']) > 0) {
      $this->i18nMode = $config_translation['i18n_mode'];
    }

    return parent::buildForm($form, $form_state, $origin_media_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_origin_media_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle($type = NULL): TranslatableMarkup {
    return parent::getTitle('media');
  }

}
