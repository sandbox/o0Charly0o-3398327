<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MigrateWizardMappingForm.
 *
 * Provides form to map formats editors.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class MigrateWizardMappingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected $mwManageDataService;

  /**
   * {@inheritdoc}
   */
  protected $originFormats;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The service to manage data.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              MWManageDataService $mw_manage_data_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mwManageDataService = $mw_manage_data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_type.manager'),
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $shared_configuration = $this->mwManageDataService->getMwDatabase()->get('shared_configuration');
    $current_config = $shared_configuration['origin_formats'] ?? [];

    // Get editor formats from drupal destiny.
    $current_formats = $this->entityTypeManager->getStorage('filter_format')->loadMultiple();
    $current_formats_array = [];

    foreach ($current_formats as $current_format) {
      $current_formats_array[$current_format->id()] = $current_format->label();
    }
    $current_formats_array = ['none' => 'Select a field'] + $current_formats_array;
    $this->originFormats = $this->mwManageDataService->getOriginEditorFormats();

    foreach ($this->originFormats as $origin_format) {
      $format = $origin_format['format'];
      $current_formats_value = $current_config[$format] ?? '';

      $form['format'][$format] = [
        '#type' => 'select',
        '#title' => $this->t('Origin Format  @format', ['@format' => $format]),
        '#options' => $current_formats_array,
        '#value' => $current_formats_value,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'migrate_wizard';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $user_inputs = $form_state->getUserInput();
    $array_config = [];

    foreach ($this->originFormats as $origin_format) {
      if ($user_inputs[$origin_format['format']] !== 'none') {
        $array_config[$origin_format['format']] = $user_inputs[$origin_format['format']];
      }
    }
    $shared_configuration = $this->mwManageDataService->getMwDatabase()->get('shared_configuration');
    $shared_configuration['origin_formats'] = $array_config;
    $this->mwManageDataService->getMwDatabase()->set('shared_configuration', $shared_configuration)->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_wizard_settings'];
  }

}
