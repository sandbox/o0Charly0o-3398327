<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OriginVocabularyFields.
 *
 * Provides form create and edit migration vocabularies.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class OriginVocabularyFields extends BaseFormFields {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_bundle = NULL) {
    $this->originBundle = $origin_bundle;
    $this->originType = 'taxonomy_term';
    $origin_vocabulary_entity = $this->entityTypeManager->getStorage('origin_vocabulary')->load($this->originBundle);
    $this->currentConfig = $origin_vocabulary_entity->shared_configuration;
    $this->manageFormService->setCurrentConfig($origin_vocabulary_entity->shared_configuration);
    $this->fieldsOriginBundle = $this->manageFormService->getFieldsOriginBundle($origin_vocabulary_entity, $this->originType);
    $this->manageFormService->setFieldsOriginBundle($this->fieldsOriginBundle);
    $type_fields_checkboxes = [
      'preserve_tid' => $this->t('Preserve tid'),
      'import_path_alias' => $this->t('Import path alias'),
    ];
    $this->checkboxes = array_merge($this->checkboxes, $type_fields_checkboxes);
    $form['origin_type'] = [
      '#type' => 'hidden',
      '#value' => 'vocabulary',
    ];

    $origin_database_value = $origin_vocabulary_entity->database;
    $form['origin_database'] = [
      '#type' => 'hidden',
      '#value' => $origin_database_value,
    ];

    $this->langCodes['default_lang']['value'] = $this->mwManageDataService->getOriginDefaultLangcode();

    $config_translation = $this->mwManageDataService->getOriginLanguages($form['origin_type']['#value'], $origin_vocabulary_entity->id_origin);

    foreach ($config_translation['origin_languages'] as $language) {
      $this->langCodes[$language . '_lang'] = [
        'markup' => $language,
        'value' => $language,
      ];
    }

    if (\count($config_translation['origin_languages']) > 0) {
      $this->i18nMode = $config_translation['i18n_mode'];
    }

    return parent::buildForm($form, $form_state, $origin_vocabulary_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_origin_vocabulary_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle($type = NULL): TranslatableMarkup {
    return parent::getTitle('taxonomy_term');
  }

}
