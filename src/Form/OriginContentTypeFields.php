<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class OriginContentTypeFields.
 *
 * Provides form create and edit migration content types.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class OriginContentTypeFields extends BaseFormFields {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $origin_bundle = NULL) {
    $this->originBundle = $origin_bundle;
    $this->originType = 'node';
    $origin_node_entity = $this->entityTypeManager->getStorage('origin_node')->load($this->originBundle);
    $this->currentConfig = $origin_node_entity->shared_configuration;
    $this->manageFormService->setCurrentConfig($origin_node_entity->shared_configuration);
    $this->fieldsOriginBundle = $this->manageFormService->getFieldsOriginBundle($origin_node_entity, $this->originType);
    $this->manageFormService->setFieldsOriginBundle($this->fieldsOriginBundle);
    $type_fields_checkboxes = [
      'preserve_nid' => $this->t('Preserve nid'),
      'import_users' => $this->t('Import users'),
      'import_path_alias' => $this->t('Import path alias'),
    ];
    $this->checkboxes = array_merge($this->checkboxes, $type_fields_checkboxes);
    $form['origin_type'] = [
      '#type' => 'hidden',
      '#value' => 'node',
    ];
    $form['origin_bundle'] = [
      '#type' => 'hidden',
      '#value' => $origin_node_entity->id_origin,
    ];
    $origin_database_value = $origin_node_entity->database;
    $form['origin_database'] = [
      '#type' => 'hidden',
      '#value' => $origin_database_value,
    ];
    $this->langCodes['default_lang']['value'] = $this->mwManageDataService->getOriginDefaultLangcode();
    $config_translation = $this->mwManageDataService->getOriginLanguages($form['origin_type']['#value'], str_replace('_' . $origin_database_value, '', $origin_bundle));

    foreach ($config_translation['origin_languages'] as $language) {
      $this->langCodes[$language . '_lang'] = [
        'markup' => $language,
        'value' => $language,
      ];
    }

    return parent::buildForm($form, $form_state, $origin_node_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_origin_content_type_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle($type = NULL): TranslatableMarkup {
    return parent::getTitle('node');
  }

}
