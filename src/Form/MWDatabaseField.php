<?php

namespace Drupal\migrate_wizard\Form;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MWDatabaseField.
 *
 * Provides form to edit a mw_database.
 *
 * @package Drupal\migrate_wizard\Form
 *
 * @ingroup migrate_wizard
 */
class MWDatabaseField extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $migrateWizardSettings;

  /**
   * The database fields.
   *
   * @var array
   */
  protected $databaseFields;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current id.
   *
   * @var string
   */
  protected $id;

  /**
   * MWDatabaseField constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $configFactory,) {
    $this->entityTypeManager = $entity_type_manager;
    $this->databaseFields = [
      'id' =>$this->t('Migration ID'),
      'migration_group' => $this->t('Migration group'),
      'key' => $this->t('Key'),
      'target' => $this->t('Target'),
      'host' => $this->t('Database host'),
      'database' => $this->t('Database name'),
      'username' => $this->t('Database username'),
      'password' => $this->t('Database password'),
      'prefix' => $this->t('Database prefix'),
      'driver' => $this->t('Database driver'),
      'port' => $this->t('Database port'),
      'namespace' => $this->t('Database driver namespace'),
      'collation' => $this->t('Database collation'),
      'url_files_prod' => $this->t('Production URL'),
    ];
    $this->migrateWizardSettings = $configFactory->getEditable('migrate_wizard.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $id = NULL) {
    $this->id = $id;
    $entity = $this->entityTypeManager->getStorage('mw_database')->load($id);
    $use_envs = ['key', 'target', 'host', 'database', 'username', 'password'];

    $form['use_env'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use .env'),
      '#default_value' => $entity->use_env,
    ];

    foreach ($this->databaseFields as $key => $database_field) {
      $title = $database_field;
      if (in_array($key, $use_envs)) {
        $title = $title . ' *';
      }
      $form[$key] = [
        '#type' => 'textfield',
        '#title' => $title,
        '#size' => '20',
        '#default_value' => $entity->$key,
      ];

      if ($database_field === 'id') {
        $form[$database_field]['#disabled'] = TRUE;
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    // Add delete.
    $form['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_mw_database_fields';
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): TranslatableMarkup {
    return $this->t('Edit MW Database');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Get triggering element.
    $triggering_element = $form_state->getTriggeringElement();

    if ($triggering_element['#id'] !== 'edit-delete') {
      $user_inputs = $form_state->getUserInput();
      $entity = $this->entityTypeManager->getStorage('mw_database')->load($this->id);

      $entity->set('use_env', $user_inputs['use_env']);

      foreach ($this->databaseFields as $key => $database_field) {
        if ($key !== 'id') {
          $entity->set($key, $user_inputs[$key]);
        }
      }
      $entity->save();
    }
    else {
      $entity = $this->entityTypeManager->getStorage('mw_database')->load($this->id);
      $entity->delete();
      $migrate_wizard_settings = $this->migrateWizardSettings->get('migrate_wizard_settings');
      if (isset($migrate_wizard_settings['databases'], $migrate_wizard_settings['databases'][$this->id])) {
        unset($migrate_wizard_settings['databases'][$this->id]);
        $this->migrateWizardSettings->set('migrate_wizard_settings', $migrate_wizard_settings)->save();
      }
    }
    drupal_flush_all_caches();
    $form_state->setRedirect('entity.database.list');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['migrate_wizard.settings'];
  }

}
