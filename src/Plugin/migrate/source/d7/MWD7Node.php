<?php

namespace Drupal\migrate_wizard\Plugin\migrate\source\d7;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\FieldTypeMWManager;
use Drupal\node\Plugin\migrate\source\d7\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 node source from database.
 *
 * @MigrateSource(
 *     id="mw_d7_node",
 *     source_module="node"
 * )
 */
class MWD7Node extends Node {

  /**
   * The join options between the node and the node_revisions table.
   */
  public const JOIN = '[n].[vid] = [nr].[vid]';

  /**
   * Array of bundles.
   *
   * @var array
   */
  public $bundle;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $config;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $configFactory;

  /**
   * The current config of content to import.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * Array of nid or vid destiny type.
   *
   * @var string
   */
  protected $destinyType;

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Entity\FieldTypeMWManager
   */
  protected $fieldTypeMWManager;

  /**
   * The left join table.
   *
   * @var string
   */
  protected $leftJoinTable;

  /**
   * The left table.
   *
   * @var string
   */
  protected $leftTable;

  /**
   * The left table id.
   *
   * @var string
   */
  protected $leftTableId;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * Array of machine name of origin to import.
   *
   * @var string
   */
  protected $originType;

  /**
   * The private file directory path, if any.
   *
   * @var string
   */
  protected $privatePath;

  /**
   * The public file directory path.
   *
   * @var string
   */
  protected $publicPath;

  /**
   * The available translations.
   *
   * @var array
   */
  protected $translation;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ConfigFactoryInterface $config_factory,
    FieldTypeMWManager $field_type_mw_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager, $module_handler);

    $this->state = $state;
    $current_source_config = $migration->getSourceConfiguration();

    $this->bundle = $current_source_config['bundle'];

    if (isset($current_source_config['constants']['translations']) && $current_source_config['constants']['translations']) {
      $this->bundle = str_replace('_' . $current_source_config['constants']['langcode'], '', $current_source_config['bundle']);
    }

    $this->mwDatabase = $this->entityTypeManager->getStorage('mw_database')->load($configuration['mw_database']);

    $current_entity = $this->entityTypeManager->getStorage('origin_node')->load($this->bundle . '_' . $configuration['mw_database']);
    $this->currentConfig = $current_entity->get('shared_configuration');

    $this->translation = [Language::LANGCODE_NOT_SPECIFIED, 'es'];

    if ($this->currentConfig != 0 && isset($this->currentConfig[array_key_first($this->currentConfig)]['languages'])) {
      $this->translation = [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->currentConfig[array_key_first($this->currentConfig)]['languages']['default'],
      ];
    }

    if (isset($configuration['constants']['translations']) && $configuration['constants']['translations']) {
      $this->translation = [$configuration['constants']['langcode']];
    }
    $this->destinyType = $this->currentConfig['destiny_type'];
    $this->originType = $this->currentConfig['origin_type'];
    $this->configFactory = $config_factory;
    $this->fieldTypeMWManager = $field_type_mw_manager;
    $this->publicPath = $this->variableGet('file_public_path', 'sites/default/files');
    $this->privatePath = $this->variableGet('file_private_path', NULL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field_type_mw')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids = [];
    $ids['nid']['type'] = 'integer';
    $ids['nid']['alias'] = 'n';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $prepare_row_result = parent::prepareRow($row);

    if ($row->hasSourceProperty('translations') && $row->getSourceProperty('translations')) {
      $row->setSourceProperty('default_langcode', 0);
    }
    if ($row->hasSourceProperty('alias') && $row->getSourceProperty('alias') && strpos($row->getSourceProperty('alias'), '/') !== 0) {
      $row->setSourceProperty('alias', '/' . $row->getSourceProperty('alias'));
    }
    // Get field API field values.
    foreach (array_keys($this->getFields($this->currentConfig['origin_type'], $this->bundle)) as $field) {

      $type_origin = reset($this->currentConfig)[$field]['type'];
      $type_destiny = reset($this->currentConfig)[$field]['destiny_type'];

      if (($type_origin != "" && $type_destiny != "") && ($type_destiny === 'paragraph')) {
        $new_field_source = [];
        $field_source = $row->getSourceProperty($field);

        foreach ($field_source as $field_source_key => $field_source_value) {

          switch ($type_origin) {
            case 'field_collection':
            case 'paragraphs':
              $new_field_source[$field_source_key] = [
                'item_id' => $field_source_value['value'],
                'revision_id' => $field_source_value['revision_id'],
              ];
              break;

            case 'entityreference':
              $new_field_source[$field_source_key] = ['nid' => $field_source_value['target_id']];
              break;
          }
        }
        $row->setSourceProperty($field, $new_field_source);
      }
    }

    foreach ($this->currentConfig[array_key_first($this->currentConfig)] as $field_name => $data_field) {
      if (isset($data_field['type']) && $data_field['destiny'] !== 'none') {
        switch ($data_field['type']) {
          case 'image':
            $new_value = [];

            foreach ($row->getSourceProperty($field_name) as $key => $value) {
              $query_uri = $this->select('file_managed', 'fm');
              $query_uri->fields('fm', [
                'uri',
                'filename',
                'filemime',
                'status',
                'timestamp',
                'uid',
              ]);
              $query_uri->condition('fm.fid', $value['fid']);
              $results = $query_uri->execute()->fetch();
              $new_value[$key] = $value;
              $new_value[$key]['uri'] = $results['uri'];
              $new_value[$key]['uid'] = $results['uid'];
              $new_value[$key]['status'] = $results['status'];
              $new_value[$key]['type'] = $data_field['type'];
              $new_value[$key]['filename'] = $results['filename'];
              $new_value[$key]['filemime'] = $results['filemime'];
              $new_value[$key]['timestamp'] = $results['timestamp'];
              $new_value[$key]['source_base_path'] = $this->configuration['constants']['source_base_path'];

              // Compute the filepath property, which is a physical
              // representation of the URI relative to the Drupal root.
              $path = str_replace([
                'public:/',
                'private:/',
              ], [
                $this->publicPath,
                $this->privatePath,
              ], $results['uri']);
              // At this point, $path could be an absolute path or a relative
              // path, depending on how the scheme's variable was set.
              // So we need to shear out the source_base_path in order
              // to make them all relative.
              $path = preg_replace('#' . preg_quote($this->configuration['constants']['source_base_path']) . '#', '', $path, 1);
              $new_value[$key]['filepath'] = $path;
              if (isset($data_field["media_type"])) {
                $new_value[$key]['bundle'] = $data_field["media_type"];
                $new_value[$key][$data_field["media_field"]] = $data_field['media_field'] ?? NULL;
              }
            }
            $row->setSourceProperty($field_name, $new_value);

            break;

          case 'file':
            $new_value = [];

            foreach ($row->getSourceProperty($field_name) as $key => $value) {
              $query_uri = $this->select('file_managed', 'fm');
              $query_uri->fields('fm', [
                'uri',
                'filename',
                'filemime',
                'status',
                'timestamp',
                'uid',
              ]);
              $query_uri->condition('fm.fid', $value['fid']);
              $results = $query_uri->execute()->fetch();
              $new_value[$key] = $value;
              $new_value[$key]['uri'] = $results['uri'];
              $new_value[$key]['uid'] = $results['uid'];
              $new_value[$key]['status'] = $results['status'];
              $new_value[$key]['type'] = $data_field['type'];
              $new_value[$key]['filename'] = $results['filename'];
              $new_value[$key]['filemime'] = $results['filemime'];
              $new_value[$key]['timestamp'] = $results['timestamp'];
              $new_value[$key]['source_base_path'] = $this->configuration['constants']['source_base_path'];
              // Compute the filepath property, which is a physical
              // representation of the URI relative to the Drupal root.
              $path = str_replace([
                'public:/',
                'private:/',
              ], [
                $this->publicPath,
                $this->privatePath,
              ], $results['uri']);
              // At this point, $path could be an absolute path or a relative
              // path, depending on how the scheme's variable was set.
              // So we need to shear out the source_base_path in order
              // to make them all relative.
              $path = preg_replace('#' . preg_quote($this->configuration['constants']['source_base_path']) . '#', '', $path, 1);
              $new_value[$key]['filepath'] = $path;
              if (isset($data_field["media_type"])) {
                $new_value[$key]['bundle'] = $data_field["media_type"];
                $new_value[$key][$data_field["media_field"]] = $data_field['media_field'] ?? NULL;
              }
            }
            $row->setSourceProperty($field_name, $new_value);
            break;

          case 'list_text':
            if ($data_field['destiny_type'] === 'entity_reference') {
              $this->listTextToTaxonomy($row, $field_name, $data_field);
            }
            break;

          case 'entityreference':
            if ($data_field['destiny_type'] === 'entity_reference_revisions') {
              $config_dependency = $this->config->get(reset($data_field['dependencies']));

              if ($config_dependency['origin_type'] === 'node' && $config_dependency['destiny_type'] === 'paragraph') {
                $new_values = [];
                $values = $row->getSourceProperty($field_name);
                foreach ($values as $key => $value) {
                  $new_values[$key] = ['nid' => reset($value)];
                }
                $row->setSourceProperty($field_name, $new_values);
              }
            }
            if ($data_field['destiny_type'] === 'entity_reference') {
              $new_values = [];
              $values = $row->getSourceProperty($field_name);
              foreach ($values as $key => $value) {
                $new_values[$key] = ['tid' => reset($value)];
              }
              $row->setSourceProperty($field_name, $new_values);
            }
            break;
        }
      }
    }

    return $prepare_row_result;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->leftTable = 'n';
    $this->leftTableId = 'nid';
    $this->entityType = 'node';
    $this->leftJoinTable = $this->getLeftJoinTable();
    $query = parent::query();

    if ($this->currentConfig['origin_type'] === 'node') {
      $query->condition('n.language', $this->translation, 'in');
    }

    if (isset($this->currentConfig['import_path_alias']) && $this->currentConfig['import_path_alias'] == 1 && !\array_key_exists('ual', $query->getTables())) {
      $this->queryEntityUrlAlias($query);
    }

    if (isset($this->currentConfig['source_type_date_field']) && $this->currentConfig['source_type_date_field'] !== '') {
      $this->addFromCreate($query, strtotime($this->currentConfig['source_type_date_field']));
    }

    if (isset($this->currentConfig['import_only_ids']) && $this->currentConfig['import_only_ids'] !== '') {
      $this->importOnlyIds($query);
    }

    if (isset($this->currentConfig['limit_import']) && $this->currentConfig['limit_import'] !== '') {
      $this->limitImport($query);
    }

    return $query;
  }

  /**
   * Add date condition to query from create timestamp.
   */
  protected function addFromCreate(&$query, $timestamp): void {
    $query->condition('created', $timestamp, '>=');
  }

  /**
   * Get left join table string.
   */
  protected function getLeftJoinTable(): string {
    return "{$this->leftTable}.{$this->leftTableId}";
  }

  /**
   * Add to the query the condition to import only the ids specified.
   */
  protected function importOnlyIds(&$query): void {
    switch ($this->currentConfig['origin_type']) {
      case 'node':
        $query->condition('n.nid', explode(',', $this->currentConfig['import_only_ids']), 'IN');
        break;

      case 'taxonomy_term':
        $query->condition('ttd.tid', explode(',', $this->currentConfig['import_only_ids']), 'IN');
        break;

      case 'user':
        $query->condition('u.uid', explode(',', $this->currentConfig['import_only_ids']), 'IN');
        break;
    }
  }

  /**
   * Add to the query the limit to import.
   */
  protected function limitImport(&$query): void {
    $query->range(0, $this->currentConfig['limit_import']);

    $query->orderBy($this->leftJoinTable, 'ASC');
  }

  /**
   * Add to query the url alias.
   */
  protected function queryEntityUrlAlias(&$query): void {
    $translations = implode("','", $this->translation);
    $id__left = $this->leftJoinTable;

    if ($this->leftJoinTable === 'tv.vid') {
      $id__left = 'ttd.tid';
    }

    $source = $this->entityType;

    if ($this->entityType === 'taxonomy_term') {
      $source = 'taxonomy/term';
    }

    $query->leftJoin('url_alias', 'ual', "CONCAT('{$source}/', {$id__left}) = ual.source AND ual.pid = (SELECT MAX(ual2.pid) FROM url_alias as ual2 WHERE ual2.source=CONCAT('{$source}/', {$id__left})) AND ual.language in('{$translations}')");
    $query->fields('ual', ['alias']);
    $query->groupBy('ual.alias');
  }

  /**
   * Listtext to taxonomy.
   *
   * If in origin have a list_text field and the destiny is a taxonomy term,
   * this function will convert the value to taxonomy term.
   */
  private function listTextToTaxonomy(&$row, $field_name, $data_field): void {
    // Load taxonomy term.
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties(['name' => $row->getSourceProperty($field_name . '_value')]);
    $term = reset($term);

    if ($term === FALSE) {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->create([
        'name' => $row->getSourceProperty($field_name . '_value'),
        'vid' => $data_field['destiny'],
      ]);
      $term->save();
    }
    $row->setSourceProperty($field_name . '_value', $term->id());
  }

}
