<?php

namespace Drupal\migrate_wizard\Plugin\migrate\source\d7;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\FieldTypeMWManager;
use Drupal\paragraphs\Plugin\migrate\source\d7\ParagraphsItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\Language;

/**
 * Paragraphs Item source plugin.
 *
 * Available configuration keys:
 * - bundle: (optional) If supplied, this will only return paragraphs
 *   of that particular type.
 *
 * @MigrateSource(
 *     id="mw_d7_paragraphs_item",
 *     source_module="paragraphs",
 * )
 */
class MWD7ParagraphsItem extends ParagraphsItem {

  /**
   * Array of bundles.
   *
   * @var array
   */
  public $bundle;

  /**
   * Query Sql to add conditions.
   *
   * @var Query
   */
  public $query;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $config;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $configFactory;

  /**
   * The current config of content to import.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * The default langcode.
   *
   * @var string
   */
  protected $defaultLangcode;

  /**
   * Array of nid or vid destiny type.
   *
   * @var string
   */
  protected $destinyType;

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Entity\FieldTypeMWManager
   */
  protected $fieldTypeMWManager;

  /**
   * The left join table.
   *
   * @var string
   */
  protected $leftJoinTable;

  /**
   * The left table.
   *
   * @var string
   */
  protected $leftTable;

  /**
   * The left join table id.
   *
   * @var string
   */
  protected $leftTableId;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * Array of machine name of origin to import.
   *
   * @var string
   */
  protected $originType;

  /**
   * The available translations.
   *
   * @var array
   */
  protected $translation;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FieldTypeMWManager $field_type_mw_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);

    $this->state = $state;
    $current_source_config = $migration->getSourceConfiguration();
    $this->config = $config_factory->getEditable('migrate_wizard.settings');

    $this->bundle = $current_source_config['field_name'];
    $this->mwDatabase = $this->entityTypeManager->getStorage('mw_database')->load($configuration['mw_database']);
    $current_entity = $this->entityTypeManager->getStorage('origin_paragraph_item')->load($this->bundle . '_' . $configuration['mw_database']);
    $this->currentConfig = $current_entity->get('shared_configuration');

    $this->defaultLangcode = $configuration['constants']['default_langcode'];

    $this->translation = [Language::LANGCODE_NOT_SPECIFIED];

    if (isset($this->currentConfig[array_key_first($this->currentConfig)]['languages'])) {
      $this->translation = [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->currentConfig[array_key_first($this->currentConfig)]['languages']['default'],
      ];
    }

    if (isset($configuration['constants']['translations']) && $configuration['constants']['translations']) {
      $this->translation = [$configuration['constants']['langcode']];
    }
    $this->configFactory = $config_factory;
    $this->fieldTypeMWManager = $field_type_mw_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field_type_mw')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $prepare_row_result = parent::prepareRow($row);
    $query_delta = $this->select('field_data_' . $row->getSourceProperty('field_name'), 'fdp');
    $query_delta->fields('fdp', ['delta']);
    $query_delta->condition('fdp.' . $row->getSourceProperty('field_name') . '_value', $row->getSourceProperty('item_id'));
    $delta = $query_delta->execute()->fetchField();
    $row->setSourceProperty('delta_paragraph', $delta);

    if (isset($this->configuration['constants']['translations']) && $this->configuration['constants']['translations']) {
      $this->getIdTranslationLookup(
        $row,
        $row->getSourceProperty('parent_type'),
        $row->getSourceProperty('field_name'),
        $row->getSourceProperty('item_id')
      );
    }

    if ($row->hasSourceProperty('parent_type')) {
      $language = $this->getLangcode($row->getSourceProperty('parent_type'), $row->getSourceProperty('field_name'), $row->getSourceProperty('item_id'));
      if ($language) {
        $row->setSourceProperty('language', $language);
      }
    }

    [
      'bundle' => $bundle,
    ] = $row->getSource();

    // Get field API field values.
    foreach (array_keys($this->getFields('paragraphs_item', $bundle)) as $field) {
      if (isset(reset($this->currentConfig)[$field]) && reset($this->currentConfig)[$field]['type'] === 'paragraphs') {
        $new_field_source = [];
        $field_source = $row->getSourceProperty($field);

        foreach ($field_source as $field_source_key => $field_source_value) {
          $new_field_source[$field_source_key] = [
            'item_id' => $field_source_value['value'],
            'revision_id' => $field_source_value['revision_id'],
          ];
        }
        $row->setSourceProperty($field, $new_field_source);
      }
    }

    foreach ($this->currentConfig[array_key_first($this->currentConfig)] as $field_name => $data_field) {
      if (isset($data_field['type']) && $data_field['destiny'] !== 'none') {
        switch ($data_field['type']) {
          case 'image':
            $new_value = [];
            foreach ($row->getSourceProperty($field_name) as $key => $value) {
              $query_uri = $this->select('file_managed', 'fm');
              $query_uri->fields('fm', ['uri']);
              $query_uri->leftJoin('field_data_field_file_image_alt_text', 'ffat', 'fm.fid = ffat.entity_id');
              $query_uri->fields('ffat', ['field_file_image_alt_text_value']);
              $query_uri->leftJoin('field_data_field_file_image_title_text', 'fftt', 'fm.fid = fftt.entity_id');
              $query_uri->fields('fftt', ['field_file_image_title_text_value']);
              $query_uri->condition('fm.fid', $value['fid']);
              $results = $query_uri->execute()->fetch();
              $new_value[$key] = $value;
              if ($results['field_file_image_alt_text_value']) {
                $new_value[0]['alt'] = $results['field_file_image_alt_text_value'];
              }
              if ($results['field_file_image_title_text_value']) {
                $new_value[0]['title'] = $results['field_file_image_title_text_value'];
              }
              $new_value[$key]['uri'] = $results['uri'];
              $new_value[$key]['type'] = $data_field['type'];
            }
            $row->setSourceProperty($field_name, $new_value);
            break;

          case 'file':
            $new_value = [];
            foreach ($row->getSourceProperty($field_name) as $key => $value) {
              $query_uri = $this->select('file_managed', 'fm');
              $query_uri->fields('fm', ['uri']);
              $query_uri->condition('fm.fid', $value['fid']);
              $uri = $query_uri->execute()->fetchField();
              $new_value[$key] = $value;
              $new_value[$key]['uri'] = $uri;
              $new_value[$key]['type'] = $data_field['type'];
            }
            $row->setSourceProperty($field_name, $new_value);
            break;

          case 'datetime':
            $date = $row->getSourceProperty($field_name . '_value');

            if (isset($data_field['format']) && $data_field['format'] !== '' && $date !== '') {
              $date = DrupalDateTime::createFromFormat('Y-m-d H:i:s', $date);
              $date->format($data_field['format']);
              $row->setSourceProperty($field_name . '_value', $date->format($data_field['format']));
            }
            break;
        }
      }
    }
    return $prepare_row_result;
  }

  /**
   * If is a translation, it will lookup for the id.
   */
  private function getIdTranslationLookup($row, $parent_type, $field_name, $item_id, &$array_tree = NULL): void {
    if ($parent_type === 'paragraphs_item') {
      if (!$array_tree) {
        $array_tree[] = [
          'entity_type' => 'paragraphs_item',
          'bundle' => $parent_type,
          'entity_id' => $item_id,
          'field_name' => $field_name,
          'delta' => $row->getSourceProperty('delta_paragraph'),
        ];
      }

      $query = $this->select('field_data_' . $field_name, 'fdc')
        ->fields('fdc', ['entity_type', 'bundle', 'entity_id', 'delta']);
      $query->condition('fdc.' . $field_name . '_value', $item_id);
      $result = $query->execute()->fetchAssoc();
      if ($result['entity_type'] === 'paragraphs_item') {
        $query2 = $this->select('paragraphs_item', 'pi');
        $query2->fields('pi', ['field_name']);
        $query2->condition('pi.item_id', $result['entity_id']);
        $result2 = $query2->execute()->fetchAssoc();
        $result = array_merge($result, $result2);
      }
      $array_tree[] = $result;

      if ($result && $result['entity_type'] === 'paragraphs_item') {
        $this->getIdTranslationLookup($row, $result['entity_type'], $result['field_name'], $result['entity_id'], $array_tree);
      }
      else {
        $query = $this->select('field_data_' . $array_tree[0]['field_name'], 'fdc')
          ->fields('fdc', [$array_tree[0]['field_name'] . '_value'])
          ->condition('fdc.delta', $array_tree[0]['delta']);

        foreach ($array_tree as $key => $join_tree) {
          if ($key === 0) {
            continue;
          }
          $suffix = $key - 1;

          if ($suffix === 0) {
            $suffix = '';
          }
          if ($join_tree['entity_type'] === 'paragraphs_item') {
            $query->leftJoin('field_data_' . $join_tree['field_name'], 'fdc' . $key, "fdc{$key}.{$join_tree['field_name']}_value = fdc{$suffix}.entity_id");
          }
          elseif ($join_tree['entity_type'] === 'node') {
            $query_tnid = $this->select('node', 'n');
            $query_tnid->condition('n.nid', $join_tree['entity_id']);
            $query_tnid->leftJoin('node', 'n2', 'n2.nid = n.tnid AND n2.nid!= n.nid');
            $query_tnid->addField('n2', 'nid', 'n2_nid');
            $nid = $query_tnid->execute()->fetchField();
            if ($nid) {
              $query->leftJoin('node', 'n', "fdc{$suffix}.entity_id = n.nid");
              $query->condition('n.nid', $nid);
              $id_lookup = $query->execute()->fetchField();
              $row->setSourceProperty('id_lookup', $id_lookup);
            }
          }
        }
      }
    }
    elseif ($parent_type === 'node') {
      $query_tnid = $this->select('node', 'n');
      $query_tnid->condition('n.nid', $row->getSourceProperty('parent_id'));
      $query_tnid->leftJoin('node', 'n2', 'n2.nid = n.tnid AND n2.nid!= n.nid');
      $query_tnid->leftJoin('field_data_' . $row->getSourceProperty('field_name'), 'fdf', 'fdf.entity_id = n2.nid AND fdf.delta = ' . $row->getSourceProperty('delta_paragraph'));
      $query_tnid->addField('fdf', $row->getSourceProperty('field_name') . '_value', 'fsf_val');
      $nid = $query_tnid->execute()->fetchField();
      if ($nid) {
        $row->setSourceProperty('id_lookup', $nid);
      }
    }
  }

  /**
   * Get the langcode of the node that contains the field collection in origin.
   */
  private function getLangcode($parent_type, $field_name, $item_id, $depth = 0) {
    if ($parent_type === 'paragraphs_item' || $depth === 0) {
      $query = $this->select('field_data_' . $field_name, 'fdc')
        ->fields('fdc', ['entity_type', 'bundle', 'entity_id']);
      $query->condition('fdc.' . $field_name . '_value', $item_id);
      $query->leftJoin('paragraphs_item', 'pi', 'fdc.entity_id = pi.item_id');
      $query->addField('pi', 'field_name');
      $result = $query->execute()->fetchAssoc();
      return $this->getLangcode($result['entity_type'], $result['field_name'], $result['entity_id'], $depth + 1);
    }
    $query = $this->select('node', 'n');
    $query->condition('nid', $item_id);
    $query->addField('n', 'language', 'language');
    if (isset($this->configuration['constants']['translations']) && $this->configuration['constants']['translations']) {
      $query->condition('n.language', $this->configuration['constants']['langcode']);
    }
    else {
      $query->condition('n.language', [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->configuration['constants']['default_langcode'],
      ], 'in');
    }
    return $query->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'item_id' => [
        'type' => 'integer',
        'alias' => 'p',
      ],
      'revision_id' => [
        'type' => 'integer',
        'alias' => 'p',
      ],
    ];
  }

}
