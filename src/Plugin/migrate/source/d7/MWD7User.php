<?php

namespace Drupal\migrate_wizard\Plugin\migrate\source\d7;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\FieldTypeMWManager;
use Drupal\user\Plugin\migrate\source\d7\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Drupal 7 user source from database.
 *
 * For available configuration keys, refer to the parent classes.
 *
 * @see \Drupal\migrate\Plugin\migrate\source\SqlBase
 * @see \Drupal\migrate\Plugin\migrate\source\SourcePluginBase
 *
 * @MigrateSource(
 *     id="mw_d7_user",
 *     source_module="user"
 * )
 */
class MWD7User extends User {

  /**
   * Array of bundles.
   *
   * @var array
   */
  public $bundle;

  /**
   * Query Sql to add conditions.
   *
   * @var Query
   */
  public $query;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $config;

  /**
   * The config of content type to import.
   *
   * @var string
   */
  protected $configFactory;

  /**
   * The current config of content to import.
   *
   * @var array
   */
  protected $currentConfig;

  /**
   * Array of nid or vid destiny type.
   *
   * @var string
   */
  protected $destinyType;

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The field type manager.
   *
   * @var \Drupal\Core\Entity\FieldTypeMWManager
   */
  protected $fieldTypeMWManager;

  /**
   * The left join table.
   *
   * @var string
   */
  protected $leftJoinTable;

  /**
   * The left table.
   *
   * @var string
   */
  protected $leftTable;

  /**
   * The left table id.
   *
   * @var string
   */
  protected $leftTableId;

  /**
   * The current mw_database.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabase;

  /**
   * Array of machine name of origin to import.
   *
   * @var string
   */
  protected $originType;

  /**
   * Array of translation.
   *
   * @var array
   */
  protected $translation;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MigrationInterface $migration,
    StateInterface $state,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FieldTypeMWManager $field_type_mw_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_type_manager);

    $this->state = $state;
    $current_source_config = $migration->getSourceConfiguration();
    $this->config = $config_factory->getEditable('migrate_wizard.settings');
    $this->bundle = $current_source_config['bundle'];

    if (isset($current_source_config['constants']['translations']) && $current_source_config['constants']['translations']) {
      $this->bundle = str_replace('_' . $current_source_config['constants']['langcode'], '', $current_source_config['bundle']);
    }
    $this->mwDatabase = $this->entityTypeManager->getStorage('mw_database')->load($configuration['mw_database']);
    $this->currentConfig = $this->mwDatabase->shared_configuration['user_migration_config'];

    $this->translation = [Language::LANGCODE_NOT_SPECIFIED, 'es'];

    if (isset($this->currentConfig[array_key_first($this->currentConfig)]['languages'])) {
      $this->translation = [
        Language::LANGCODE_NOT_SPECIFIED,
        $this->currentConfig[array_key_first($this->currentConfig)]['languages']['default'],
      ];
    }

    if (isset($configuration['constants']['translations']) && $configuration['constants']['translations']) {
      $this->translation = [$configuration['constants']['langcode']];
    }
    $this->destinyType = $this->currentConfig['destiny_type'];
    $this->originType = $this->currentConfig['origin_type'];
    $this->configFactory = $config_factory;
    $this->fieldTypeMWManager = $field_type_mw_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field_type_mw')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $prepare_row_result = parent::prepareRow($row);

    // Get field API field values.
    foreach (array_keys($this->getFields($this->currentConfig['origin_type'], $this->bundle)) as $field) {
      if (isset(reset($this->currentConfig)[$field]) && reset($this->currentConfig)[$field]['type'] === 'field_collection') {
        $new_field_source = [];
        $field_source = $row->getSourceProperty($field);

        foreach ($field_source as $field_source_key => $field_source_value) {
          $new_field_source[$field_source_key] = ['item_id' => $field_source_value['value']];
        }
        $row->setSourceProperty($field, $new_field_source);
      }
    }

    foreach ($this->currentConfig[array_key_first($this->currentConfig)] as $field_name => $data_field) {
      if (isset($data_field['type']) && $data_field['destiny'] !== 'none') {
        switch ($data_field['type']) {
          case 'image':
            $new_value = [];

            if ($field_name !== 'picture') {
              foreach ($row->getSourceProperty($field_name) as $key => $value) {
                $query_uri = $this->select('file_managed', 'fm');
                $query_uri->fields('fm', ['uri']);
                $query_uri->leftJoin('field_data_field_file_image_alt_text', 'ffat', 'fm.fid = ffat.entity_id');
                $query_uri->fields('ffat', ['field_file_image_alt_text_value']);
                $query_uri->leftJoin('field_data_field_file_image_title_text', 'fftt', 'fm.fid = fftt.entity_id');
                $query_uri->fields('fftt', ['field_file_image_title_text_value']);
                $query_uri->condition('fm.fid', $value['fid']);
                $results = $query_uri->execute()->fetch();
                $new_value[$key] = $value;

                if ($results['field_file_image_alt_text_value']) {
                  $new_value[0]['alt'] = $results['field_file_image_alt_text_value'];
                }

                if ($results['field_file_image_title_text_value']) {
                  $new_value[0]['title'] = $results['field_file_image_title_text_value'];
                }
                $new_value[$key]['uri'] = $results['uri'];
                $new_value[$key]['type'] = $data_field['type'];
              }
            }
            else {
              $fid = $row->getSourceProperty($field_name);

              if ($fid !== 0) {
                $query_uri = $this->select('file_managed', 'fm');
                $query_uri->fields('fm', ['uri']);
                $query_uri->condition('fm.fid', $row->getSourceProperty($field_name));
                $uri = $query_uri->execute()->fetchField();
                $new_value[0] = [
                  'fid' => $fid,
                  'alt' => '',
                  'title' => '',
                  'width' => '',
                  'height' => '',
                ];
                $new_value[0]['uri'] = $uri;
                $new_value[0]['type'] = $data_field['type'];
              }
            }

            if (\count($new_value) > 0) {
              $row->setSourceProperty($field_name, $new_value);
            }

            break;

          case 'file':
            $new_value = [];

            if ($field_name !== 'picture') {
              foreach ($row->getSourceProperty($field_name) as $key => $value) {
                $query_uri = $this->select('file_managed', 'fm');
                $query_uri->fields('fm', ['uri']);
                $query_uri->condition('fm.fid', $value['fid']);
                $uri = $query_uri->execute()->fetchField();
                $new_value[$key] = $value;
                $new_value[$key]['uri'] = $uri;
                $new_value[$key]['type'] = $data_field['type'];
              }
            }
            else {
              $fid = $row->getSourceProperty($field_name);

              if ($fid !== 0) {
                $query_uri = $this->select('file_managed', 'fm');
                $query_uri->fields('fm', ['uri']);
                $query_uri->condition('fm.fid', $row->getSourceProperty($field_name));
                $uri = $query_uri->execute()->fetchField();
                $new_value[0] = [
                  'fid' => $fid,
                  'alt' => '',
                  'title' => '',
                  'width' => '',
                  'height' => '',
                ];
                $new_value[0]['uri'] = $uri;
                $new_value[0]['type'] = $data_field['type'];
              }
            }

            if (\count($new_value) > 0) {
              $row->setSourceProperty($field_name, $new_value);
            }

            break;

          case 'datetime':
            $date = $row->getSourceProperty($field_name . '_value');

            if (isset($data_field['format']) && $data_field['format'] !== '' && $date !== '') {
              $date = DrupalDateTime::createFromFormat('Y-m-d H:i:s', $date);
              $date->format($data_field['format']);
              $row->setSourceProperty($field_name . '_value', $date->format($data_field['format']));
            }

            break;

          // Case 'list_text':
          // if ($data_field['destiny_type'] == 'entity_reference') {
          // $this->listTextToTaxonomy($row, $field_name, $data_field);
          // }
          // break;.
        }
      }
    }

    if ($row->getSourceProperty('alias') !== NULL && substr($row->getSourceProperty('alias'), 0, 1) !== '/') {
      $row->setSourceProperty('alias', '/' . $row->getSourceProperty('alias'));
    }

    return $prepare_row_result;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->leftTable = 'u';
    $this->leftTableId = 'uid';
    $this->entityType = 'user';
    $this->leftJoinTable = $this->getLeftJoinTable();

    $query = parent::query();

    $query->condition('u.uid', 1, '<>');
    if (isset($this->currentConfig['import_path_alias']) && $this->currentConfig['import_path_alias'] === 1 && !\array_key_exists('ual', $query->getTables())) {
      $this->queryEntityUrlAlias($query);
    }

    if (isset($this->currentConfig['source_type_date_field']) && $this->currentConfig['source_type_date_field'] !== NULL) {
      $this->addFromCreate($query, strtotime($this->currentConfig['source_type_date_field']));
    }

    if (isset($this->currentConfig['import_only_ids']) && $this->currentConfig['import_only_ids'] !== '') {
      $this->importOnlyIds($query);
    }

    if (isset($this->currentConfig['limit_import']) && $this->currentConfig['limit_import'] !== '') {
      $this->limitImport($query);
    }

    if (isset($this->currentConfig['import_cas']) && $this->currentConfig['import_cas']) {
      $this->queryUserCas($query);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFromCreate(&$query, $timestamp): void {
    $query->condition('created', $timestamp, '>=');
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeftJoinTable(): string {
    return "{$this->leftTable}.{$this->leftTableId}";
  }

  /**
   * {@inheritdoc}
   */
  protected function importOnlyIds(&$query): void {
    switch ($this->currentConfig['origin_type']) {
      case 'node':
        $query->condition('n.nid', explode(',', $this->currentConfig['import_only_ids']), 'IN');

        break;

      case 'taxonomy_term':
        $query->condition('ttd.tid', explode(',', $this->currentConfig['import_only_ids']), 'IN');

        break;

      case 'user':
        $query->condition('u.uid', explode(',', $this->currentConfig['import_only_ids']), 'IN');

        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function limitImport(&$query): void {
    $query->range(0, $this->currentConfig['limit_import']);

    $query->orderBy($this->leftJoinTable, 'ASC');
  }

  /**
   * {@inheritdoc}
   */
  protected function queryEntityUrlAlias(&$query): void {
    $translations = implode("','", $this->translation);
    $id__left = $this->leftJoinTable;
    $source = $this->entityType;

    $query->leftJoin('url_alias', 'ual', "CONCAT('{$source}/', {$id__left}) = ual.source AND ual.pid = (SELECT MAX(ual2.pid) FROM url_alias as ual2 WHERE ual2.source=CONCAT('{$source}/', {$id__left})) AND ual.language in('{$translations}')");
    $query->fields('ual', ['alias']);
  }

  /**
   * Add to query user cas join.
   *
   * @param Query $query
   *   The query.
   */
  protected function queryUserCas(&$query): void {
    $query->leftJoin('cas_user', 'cu', "{$this->leftJoinTable} = cu.uid");
    $query->fields('cu', ['cas_name']);
  }

}
