<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\link\Plugin\migrate\process\FieldLink;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Media concat data.
 *
 * @MigrateProcessPlugin(
 *     id="concat_data"
 * )
 */
class ConcatData extends FieldLink {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_value = [];

    foreach ($value as $val) {
      if (\is_array($val)) {
        $new_value = array_merge($new_value, $val);
      }
    }
    return $new_value;
  }

}
