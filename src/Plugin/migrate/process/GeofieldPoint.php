<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Geofield explode.
 *
 * @MigrateProcessPlugin(
 *     id="geofield_point"
 * )
 */
class GeofieldPoint extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return 'POINT (' . rtrim($value[0], '0') . ' ' . rtrim($value[1], '0') . ')';
  }

}
