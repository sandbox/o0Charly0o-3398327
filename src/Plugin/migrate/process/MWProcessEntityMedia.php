<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_wizard\Plugin\migrate\destination\MWEntityMedia;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Geofield explode.
 *
 * @MigrateProcessPlugin(
 *     id="mw_process_entity_media"
 * )
 */
class MWProcessEntityMedia extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityMediaPlugin;

  /**
   * Constructs a new MWProcessEntityMedia object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate_wizard\Plugin\migrate\destination\MWEntityMedia $entity_media_plugin
   *   The entity media plugin.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, MWEntityMedia $entity_media_plugin) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityMediaPlugin = $entity_media_plugin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.migrate.destination')->createInstance('mwentity:media', $configuration, $migration)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $destination = $row->getSourceProperty('uri');

    if (empty($destination)) {
      throw new MigrateException('Destination property uri not provided');
    }
    $entity = $this->entityMediaPlugin->getEntity($row, []);
    $entity->save();

    return $entity->id();
  }

}
