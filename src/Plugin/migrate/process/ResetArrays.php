<?php

namespace Drupal\migrate_wizard\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Geofield explode.
 *
 * @MigrateProcessPlugin(
 *     id="reset_arrays"
 * )
 */
class ResetArrays extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $new_values = [];

    if (\is_array($value)) {
      foreach ($value as $key => $val) {
        if (\is_array($val)) {
          $new_values[$key] = reset($val);
        }
      }
    }

    return $new_values;
  }

}
