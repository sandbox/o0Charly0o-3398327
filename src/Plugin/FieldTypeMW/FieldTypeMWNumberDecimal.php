<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'number_decimal' field type.
 *
 * @FieldType(
 *     id="number_decimal",
 * )
 */
class FieldTypeMWNumberDecimal extends FieldTypeMWGenericText {

}
