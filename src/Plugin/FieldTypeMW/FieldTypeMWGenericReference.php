<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'generic_reference' field type.
 *
 * @FieldType(
 *     id="generic_reference",
 * )
 */
class FieldTypeMWGenericReference extends FieldTypeMWBase {

  /**
   * Generate the migration of reference field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    switch ($data_field['type']) {
      case 'taxonomy_term_reference':
        $source = 'tid';

        break;

      case 'node_reference':
        $source = 'nid';

        break;

      case 'entityreference':
        if ($data_field["target_type"] == 'taxonomy_term') {
          $source = 'tid';
        }
        else {
          $source = 'nid';
        }

        break;

      default:
        $source = 'target_id';

        break;
    }

    $array_config[$data_field['destiny']][] = [
      'plugin' => 'skip_on_empty',
      'source' => $origin_field,
      'method' => 'process',
    ];

    if ($source !== 'target_id') {
      $array_config[$data_field['destiny']][] = [
        'plugin' => 'sub_process',
        'method' => 'process',
        'process' => [
          'target_id' => [
            'plugin' => 'migration_lookup',
            'source' => $source,
            'migration' => $data_field['migration_lookup'],
            'no_stub' => TRUE,
          ],
        ],
      ];
    }
    else {
      foreach ($data_field['migration_lookup'] as $bundle) {
        $array_config[$data_field['destiny']][] = [
          'plugin' => 'migration_lookup',
          'migration' => $bundle,
          'no_stub' => TRUE,
        ];
      }
      $array_config[$data_field['destiny']][] = [
        'plugin' => 'sub_process',
        'process' => [$source => '0', 'target_revision_id' => '1'],
      ];
    }

    $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);

    foreach ($data_field['dependencies'] as $dependency) {
      if (!\in_array($dependency, $wildcard_yml_config['migration_dependencies']['required'], TRUE) && $dependency !== $wildcard_yml_config['id']) {
        $wildcard_yml_config['migration_dependencies']['required'][] = $dependency;
      }
    }
  }

}
