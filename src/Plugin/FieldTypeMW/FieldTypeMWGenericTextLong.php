<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'generic_text_long' field type.
 *
 * @FieldType(
 *     id="generic_text_long",
 * )
 */
class FieldTypeMWGenericTextLong extends FieldTypeMWBase {

  /**
   * Generate the migration of text long field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    $origin_field_value = $origin_field;

    if ($origin_field === 'description' && $wildcard_yml_config['source']['origin_type'] === 'vocabulary' && $langcode !== NULL) {
      $origin_field_value = 'description_translated';
    }

    if ($wildcard_yml_config['source']['origin_type'] === 'taxonomy_term' && $wildcard_yml_config['destination']['plugin'] === 'entity:node' && $origin_field === 'description') {
      $array_config[$data_field['destiny'] . '/value'][] = [
        'plugin' => 'skip_on_empty',
        'source' => $origin_field_value,
        'method' => 'process',
      ];

      $array_config[$data_field['destiny'] . '/format'][] = [
        'plugin' => 'static_map',
        'source' => 'format',
        'map' => $config['origin_formats'],
        'default_value' => 'full_html',
      ];
      $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
    }
    else {
      $array_config['pseudo_'.$origin_field_value][] = [
        'plugin' => 'skip_on_empty',
        'source' => $origin_field_value,
        'method' => 'process',
      ];

      if(!isset($wildcard_yml_config['process'][$data_field['destiny']])){

        $array_config['pseudo_'.$data_field['destiny'].'_destiny'][] = [
          'plugin' => 'concat_data',
          'source' => ['@pseudo_'.$origin_field_value]
        ];

        $array_config[$data_field['destiny']][] = [
          'plugin' => 'sub_process',
          'source' => '@pseudo_'.$data_field['destiny'].'_destiny',
          'process' => [
            'value' => 'value',
            'summary' => 'summary',
            'format' => [
              'plugin' => 'static_map',
              'source' => 'format',
              'map' => $config['origin_formats'],
              'default_value' => 'full_html',
            ],
          ],
        ];
        $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
      }else{
        $wildcard_yml_config['process'] = parent::InsertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $data_field['destiny'] . '_destiny', 'pseudo_' . $origin_field_value, $array_config['pseudo_'.$origin_field_value][0]);
        $wildcard_yml_config['process']['pseudo_' . $data_field['destiny'] . '_destiny'][0]['source'][] = '@pseudo_' . $origin_field_value;
      }
    }
  }
}
