<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'generic_file' field type.
 *
 * @FieldType(
 *     id="generic_file",
 * )
 */
class FieldTypeMWGenericFile extends FieldTypeMWBase {

  /**
   * Generate the migration of file field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL) {
    if ($type_source === 'taxonomy_term' && $langcode !== NULL) {
      return TRUE;
    }
    $fields_content_type_destiny = $entityFieldManager->getFieldDefinitions($type_source, $wildcard_yml_config['process']['type']['default_value']);
    $settings_field = $fields_content_type_destiny[$data_field['destiny']]->getSettings();
    $array_config = [];

    if (\count($settings_field['handler_settings']) > 0) {
      if (!isset($wildcard_yml_config['process'][$data_field['destiny']])) {
        $array_config['pseudo_' . $origin_field][] = [
          'plugin' => 'skip_on_empty',
          'source' => $origin_field,
          'method' => 'process',
        ];

        $insert_value = self::buildFileConfig($data_field, 'fid');
        $insert_value['process']['alt'] = 'alt';
        $insert_value['process']['title'] = 'title';

        $array_config['pseudo_' . $origin_field][] = $insert_value;

        $array_config['pseudo_' . $data_field['destiny'] . '_destiny'][] = [
          'plugin' => 'concat_data',
          'source' => [
            '@pseudo_' . $origin_field,
          ],
          'method' => 'process',
        ];
        $array_config[$data_field['destiny']][] = [
          'plugin' => 'sub_process',
          'source' => '@pseudo_' . $data_field['destiny'] . '_destiny',
          'method' => 'process',
          'process' => ['target_id' => 'target_id'],
        ];
      }
      else {
        $array_config['pseudo_' . $origin_field][] = [
          'plugin' => 'skip_on_empty',
          'source' => $origin_field,
          'method' => 'process',
        ];

        $insert_value = self::buildFileConfig($data_field, 'fid');
        $insert_value['process']['alt'] = 'alt';
        $insert_value['process']['title'] = 'title';

        $array_config['pseudo_' . $origin_field][] = $insert_value;

        $wildcard_yml_config['process'] = parent::InsertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $data_field['destiny'] . '_destiny', 'pseudo_' . $origin_field, $insert_value);
        $wildcard_yml_config['process']['pseudo_' . $data_field['destiny'] . '_destiny'][0]['source'][] = '@pseudo_' . $origin_field;
      }
    }
    else {
      if (!isset($wildcard_yml_config['process'][$data_field['destiny']])) {
        if ($data_field['type'] === 'image') {
          $array_config['pseudo_' . $origin_field][] = [
            'plugin' => 'skip_on_empty',
            'source' => $origin_field,
            'method' => 'process',
          ];

          if (!isset($data_field['media_field'])) {
            $data_field['media_field'] = 0;
          }
          $insert_value = self::buildFileConfig($data_field, 'target_id');
          $insert_value['process']['alt'] = 'alt';
          $insert_value['process']['title'] = 'title';

          $array_config['pseudo_' . $origin_field][] = $insert_value;

          $array_config['pseudo_' . $data_field['destiny'] . '_destiny'][] = [
            'plugin' => 'concat_data',
            'source' => [
              '@pseudo_' . $origin_field,
            ],
            'method' => 'process',
          ];
          $array_config[$data_field['destiny']][] = [
            'plugin' => 'sub_process',
            'source' => '@pseudo_' . $data_field['destiny'] . '_destiny',
            'method' => 'process',
            'process' => ['target_id' => 'target_id'],
          ];
        }
        elseif ($data_field['type'] === 'file') {
          $array_config[$data_field['destiny']][] = [
            'plugin' => 'skip_on_empty',
            'source' => $origin_field,
            'method' => 'process',
          ];
          $insert_value = self::buildFileConfig($data_field, 'target_id');
          $insert_value['process']['description'] = 'description';
          $array_config[$data_field['destiny']][] = $insert_value;
        }
      }
      else {
        $insert_value = [
          'plugin' => 'skip_on_empty',
          'source' => $origin_field,
          'method' => 'process',
        ];
        $wildcard_yml_config['process'] = parent::InsertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $data_field['destiny'] . '_destiny', 'pseudo_' . $origin_field, $insert_value);
        $insert_value = self::buildFileConfig($data_field, 'target_id');
        $insert_value['process']['alt'] = 'alt';
        $insert_value['process']['title'] = 'title';
        $wildcard_yml_config['process'] = parent::InsertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $data_field['destiny'] . '_destiny', 'pseudo_' . $origin_field, $insert_value);
        $wildcard_yml_config['process']['pseudo_' . $data_field['destiny'] . '_destiny'][0]['source'][] = '@pseudo_' . $origin_field;
      }
    }
    $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
  }

  /**
   * Build the configuration of the file.
   */
  private static function buildFileConfig($data_field, $destiny) {
    $config_file = [
      'plugin' => 'sub_process',
      'method' => 'process',
      'process' => [
        'bundle' => $data_field['type'] ? 'bundle' : '',
        'field_name_destiny' => isset($data_field['media_field']) && $data_field['media_field'] != 0 ? $data_field['media_field'] : '',
        'source_full_path' => [
          [
            'plugin' => 'mw_concat',
            'delimiter' => '/',
            'source' => [
              'source_base_path',
              'filepath',
            ],
          ],
          [
            'plugin' => 'urlencode',
          ],
        ],
        'uri_dest' => [
          'plugin' => 'mw_file_copy',
          'source' => [
            '@source_full_path',
            'uri',
          ],
        ],
        $destiny => [
          'plugin' => 'mw_process_entity_file',
          'source' => [
            '@source_full_path',
            '@uri_dest',
            'filename',
            'filemime',
            'filesize',
            'status',
            'created',
            'changed',
            'uid',
          ],
        ],
      ],
    ];

    if ($destiny === 'fid') {
      $config_file['process']['target_id'] = [
        'plugin' => 'mw_process_entity_media',
      ];
    }

    return $config_file;
  }

}
