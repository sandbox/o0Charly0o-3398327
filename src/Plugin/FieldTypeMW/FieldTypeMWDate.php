<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'date' field type.
 *
 * @FieldType(
 *     id="date",
 * )
 */
class FieldTypeMWDate extends FieldTypeMWGenericDate {

}
