<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'range_decimal' field type.
 *
 * @FieldType(
 *     id="range_decimal",
 * )
 */
class FieldTypeMWRangeDecimal extends FieldTypeMWGenericRange {

}
