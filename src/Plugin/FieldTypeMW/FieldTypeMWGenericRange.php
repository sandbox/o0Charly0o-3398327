<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'range' field type.
 *
 * @FieldType(
 *     id="range",
 * )
 */
class FieldTypeMWGenericRange extends FieldTypeMWBase {

  /**
   * Generate the migration of range field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {
    $array_config[$data_field['destiny']][] = [
      'plugin' => 'skip_on_empty',
      'source' => [$origin_field . '_from', $origin_field . '_to'],
      'method' => 'process',
    ];
    $array_config[$data_field['destiny']][] = [
      'plugin' => 'format_range_explode',
    ];

    $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
  }

}
