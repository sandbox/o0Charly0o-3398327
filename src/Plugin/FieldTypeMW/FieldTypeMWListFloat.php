<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'list_float' field type.
 *
 * @FieldType(
 *     id="list_float",
 * )
 */
class FieldTypeMWListFloat extends FieldTypeMWGenericText {

}
