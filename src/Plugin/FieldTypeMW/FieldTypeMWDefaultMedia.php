<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'default_media' field type.
 *
 * @FieldType(
 *     id="default_media",
 * )
 */
class FieldTypeMWDefaultMedia extends FieldTypeMWBase {

  /**
   * Generate the base of migration of medias.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $array_config
   *   The configuration of the field.
   * @param string $origin_node
   *   The origin node.
   * @param string $migration_group_name
   *   The migration group name.
   * @param string $default_langcode
   *   The default language code.
   * @param null $langcode
   *   The language code.
   * @param bool $translation
   *   The translation flag.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $array_config, $origin_node, $migration_group_name, $default_langcode = NULL, $langcode = NULL, $translation = FALSE) {
    $mw_database = parent::getMwDatabase($array_config['mw_database']);
    $suffix = '';

    if ($translation) {
      $suffix = '_' . $langcode;
    }
    $wildcard_yml_config['id'] = $origin_node . '_' . $array_config['mw_database'] . $suffix;
    $wildcard_yml_config['label'] = $origin_node . $suffix;
    $wildcard_yml_config['langcode'] = $langcode;
    $wildcard_yml_config['migration_group'] = $migration_group_name;
    $wildcard_yml_config['migration_tags'][0] = $migration_group_name;
    $wildcard_yml_config['source']['key'] = $mw_database->get('key');
    $wildcard_yml_config['source']['constants']['source_base_path'] = $mw_database->url_files_prod;
    $wildcard_yml_config['process']['temp1'][0]['value'][] = str_replace('_', '/', $origin_node);
    $wildcard_yml_config['process'][$array_config['listmediafields'] . '/target_id'][] = [
      'plugin' => 'migration_lookup',
      'migration' => 'files_' . $array_config['mw_database'],
      'source' => 'fid',
    ];
    $wildcard_yml_config['migration_dependencies']['required'][] = 'files_' . $array_config['mw_database'];
    $wildcard_yml_config['destination']['default_bundle'] = array_key_first($array_config);
  }

}
