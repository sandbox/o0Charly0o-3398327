<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'list_integer' field type.
 *
 * @FieldType(
 *     id="list_integer",
 * )
 */
class FieldTypeMWListInteger extends FieldTypeMWGenericText {

}
