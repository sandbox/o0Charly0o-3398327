<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'default_node' field type.
 *
 * @FieldType(
 *     id="default_node",
 * )
 */
class FieldTypeMWDefaultNode extends FieldTypeMWBase {

  /**
   * Generate the base of migration of nodes.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $array_config
   *   The configuration of the field.
   * @param string $origin_node
   *   The origin node.
   * @param string $migration_group_name
   *   The migration group name.
   * @param string $default_langcode
   *   The default language code.
   * @param null $langcode
   *   The language code.
   * @param bool $translation
   *   The translation flag.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $array_config, $origin_node, $migration_group_name, $default_langcode = NULL, $langcode = NULL, $translation = FALSE) {
    $mw_database = parent::getMwDatabase($array_config['mw_database']);
    $suffix = '';

    if ($translation) {
      $suffix = '_' . $langcode;
    }
    $wildcard_yml_config['id'] = $origin_node . '_' . $array_config['mw_database'] . $suffix;
    $wildcard_yml_config['label'] = $origin_node . $suffix;
    $wildcard_yml_config['langcode'] = $langcode;
    $wildcard_yml_config['migration_group'] = $migration_group_name;
    $wildcard_yml_config['migration_tags'][0] = $migration_group_name;
    $wildcard_yml_config['source']['key'] = $mw_database->get('key');
    $wildcard_yml_config['source']['mw_database'] = $array_config['mw_database'];
    $wildcard_yml_config['source']['bundle'] = $origin_node . $suffix;
    $wildcard_yml_config['source']['node_type'] = $origin_node;
    $wildcard_yml_config['source']['origin_type'] = $array_config['origin_type'];
    $wildcard_yml_config['source']['constants']['langcode'] = $langcode;
    $wildcard_yml_config['source']['constants']['source_base_path'] = $mw_database->url_files_prod;

    if ($translation) {
      $wildcard_yml_config['source']['translations'] = TRUE;
      $wildcard_yml_config['source']['constants']['translations'] = TRUE;
      $wildcard_yml_config['process']['nid'] = [
        'plugin' => 'migration_lookup',
        'migration' => $origin_node . '_' . $array_config['mw_database'],
        'source' => 'tnid',
        'no_stub' => TRUE,
      ];
      $wildcard_yml_config['process']['langcode'] = 'language';
      $wildcard_yml_config['migration_dependencies']['required'] = [$origin_node . '_' . $array_config['mw_database']];
      $wildcard_yml_config['destination']['default_bundle'] = $origin_node;
      $wildcard_yml_config['destination']['translations'] = TRUE;
      $wildcard_yml_config['destination']['langcode'] = $langcode;
    }
    $wildcard_yml_config['process']['type'] = [
      'plugin' => 'default_value',
      'default_value' => array_key_first($array_config),
    ];
    $wildcard_yml_config['process']['created'] = 'created';
    $wildcard_yml_config['process']['status'] = 'status';

    if (isset($array_config['preserve_nid']) && $array_config['preserve_nid'] && !$translation) {
      $wildcard_yml_config['process']['nid'] = 'nid';
    }

    if (isset($array_config['import_users']) && $array_config['import_users']) {
      $wildcard_yml_config['process']['uid'] = [
        'plugin' => 'migration_lookup',
        'migration' => 'users_' . $array_config['mw_database'],
        'source' => 'node_uid',
        'no_stub' => TRUE,
      ];
      $wildcard_yml_config['migration_dependencies']['required'][] = 'users_' . $array_config['mw_database'];
    }

    if (isset($array_config['import_path_alias']) && $array_config['import_path_alias']) {
      $wildcard_yml_config['process']['path/pathauto'][] = [
        'plugin' => 'default_value',
        'default_value' => 0,
      ];
      $wildcard_yml_config['process']['path/alias'][] = [
        'plugin' => 'skip_on_empty',
        'source' => 'alias',
        'method' => 'process',
      ];
    }

    if ($array_config['destiny_type'] === 'taxonomy_term') {
      $wildcard_yml_config['process']['vid'] = [
        'plugin' => 'default_value',
        'default_value' => array_key_first($array_config),
      ];
      $wildcard_yml_config['destination']['plugin'] = 'entity:taxonomy_term';
    }
    elseif ($array_config['destiny_type'] === 'paragraph') {
      $wildcard_yml_config['process']['vid'] = [
        'plugin' => 'default_value',
        'default_value' => array_key_first($array_config),
      ];
      $wildcard_yml_config['destination']['plugin'] = 'mw_entity_reference_revisions:paragraph';
    }
  }

}
