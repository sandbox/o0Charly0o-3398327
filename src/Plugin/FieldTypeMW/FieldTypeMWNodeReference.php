<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'node_reference' field type.
 *
 * @FieldType(
 *     id="node_reference",
 * )
 */
class FieldTypeMWNodeReference extends FieldTypeMWGenericReference {

}
