<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'default_files' field type.
 *
 * @FieldType(
 *     id="default_files",
 * )
 */
class FieldTypeMWDefaultFiles extends FieldTypeMWBase {

  /**
   * Generate the base of migration of files.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $array_config
   *   The configuration of the field.
   * @param string $migration_group_name
   *   The migration group name.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $array_config, $migration_group_name) {
    $mw_database = parent::getMwDatabase($array_config['mw_database']);
    $wildcard_yml_config['id'] = 'files_' . $array_config['mw_database'];
    $wildcard_yml_config['label'] = 'files_' . $array_config['mw_database'];
    $wildcard_yml_config['migration_group'] = $migration_group_name;
    $wildcard_yml_config['migration_tags'][0] = $migration_group_name;
    $wildcard_yml_config['source']['key'] = $mw_database->get('key');
    $database = $mw_database->get('id');
    $wildcard_yml_config['source']['mw_database'] = $database;
    $wildcard_yml_config['source']['constants']['source_base_path'] = $mw_database->url_files_prod;

    if ($mw_database->shared_configuration['migration_files']['preserve_fid']) {
      $wildcard_yml_config['process']['fid'] = 'fid';
    }
  }

}
