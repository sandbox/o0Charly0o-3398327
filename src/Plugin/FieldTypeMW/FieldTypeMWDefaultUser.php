<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'default_user' field type.
 *
 * @FieldType(
 *     id="default_user",
 * )
 */
class FieldTypeMWDefaultUser extends FieldTypeMWBase {

  /**
   * Generate the base of migration of users.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $array_config
   *   The configuration of the field.
   * @param string $origin_node
   *   The origin node.
   * @param string $migration_group_name
   *   The migration group name.
   * @param array $array_config_roles
   *   The configuration of the roles.
   * @param null $langcode
   *   The language code.
   * @param bool $translation
   *   The translation flag.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $array_config, $origin_node, $migration_group_name, $array_config_roles, $langcode = NULL, $translation = FALSE) {
    $mw_database = parent::getMwDatabase($array_config['mw_database']);
    $suffix = '';

    if ($translation) {
      $suffix = '_' . $langcode;
    }
    $wildcard_yml_config['id'] = 'user_' . $array_config['mw_database'] . $suffix;
    $wildcard_yml_config['label'] = 'user' . $suffix;
    $wildcard_yml_config['langcode'] = $langcode;
    $wildcard_yml_config['migration_group'] = $migration_group_name;
    $wildcard_yml_config['migration_tags'][0] = $migration_group_name;
    $wildcard_yml_config['source']['key'] = $mw_database->get('key');
    $database = $mw_database->get('id');
    $wildcard_yml_config['source']['mw_database'] = $database;
    $wildcard_yml_config['source']['bundle'] = 'user' . $suffix;
    $wildcard_yml_config['source']['origin_type'] = $array_config['origin_type'];
    $wildcard_yml_config['source']['constants']['langcode'] = $langcode;

    if ($translation) {
      $wildcard_yml_config['source']['translations'] = TRUE;
      $wildcard_yml_config['source']['constants']['translations'] = TRUE;
      $wildcard_yml_config['process']['nid'] = [
        'plugin' => 'migration_lookup',
        'migration' => 'user',
        'source' => 'tnid',
        'no_stub' => TRUE,
      ];
      $wildcard_yml_config['process']['langcode'] = 'language';
      $wildcard_yml_config['destination']['default_bundle'] = 'user';
      $wildcard_yml_config['destination']['translations'] = TRUE;
      $wildcard_yml_config['destination']['langcode'] = $langcode;
    }
    $wildcard_yml_config['process']['type'] = [
      'plugin' => 'default_value',
      'default_value' => array_key_first($array_config),
    ];
    $wildcard_yml_config['process']['created'] = 'created';
    $wildcard_yml_config['process']['status'] = 'status';

    if ($array_config['preserve_uid']) {
      $wildcard_yml_config['process']['uid'] = 'uid';
    }

    if ($array_config['import_path_alias']) {
      $wildcard_yml_config['process']['path'][] = [
        'plugin' => 'skip_on_empty',
        'source' => 'alias',
        'method' => 'process',
      ];
    }

    if (isset($array_config['import_cas']) && $array_config['import_cas']) {
      $wildcard_yml_config['source']['import_cas'] = TRUE;
    }

    if (isset($array_config_roles['roles'])) {
      foreach ($array_config_roles['roles'] as $key_rol => $rol) {
        if ($rol['destiny'] !== 'none' && $rol['destiny'] !== 'create') {
          $wildcard_yml_config['process']['roles'][1]['map'][$key_rol] = $rol['destiny'];
        }
      }
    }
  }

}
