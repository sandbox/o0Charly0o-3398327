<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

use Drupal\migrate_wizard\FieldTypeMWBase;

/**
 * Provides 'daterange' field type.
 *
 * @FieldType(
 *     id="daterange",
 * )
 */
class FieldTypeMWDateRange extends FieldTypeMWBase {

  /**
   * Generate the migration of date range field type.
   *
   * @param array $wildcard_yml_config
   *   The configuration of the migration.
   * @param array $origin_field
   *   The origin field.
   * @param array $data_field
   *   The data field.
   * @param string $type_source
   *   The type source.
   * @param array $config
   *   The configuration.
   * @param object $entityFieldManager
   *   The entity field manager.
   * @param object $fieldTypeMWManager
   *   The field type manager.
   * @param string|null $langcode
   *   The langcode.
   */
  public static function getD7MigrationConfig(&$wildcard_yml_config, $origin_field, $data_field, $type_source, $config, $entityFieldManager, $fieldTypeMWManager, $langcode = NULL): void {

    $array_config['pseudo_'.$origin_field][] = [
      'plugin' => 'skip_on_empty',
      'source' => $origin_field,
      'method' => 'process',
    ];
    if(!isset($wildcard_yml_config['process'][$data_field['destiny']])){
      $array_config['pseudo_'.$data_field['destiny'].'_destiny'][] = [
        'plugin' => 'concat_data',
        'source' => ['@pseudo_'.$origin_field]
      ];

      $array_config[$data_field['destiny']][] = [
        'plugin' => 'sub_process',
        'source' => '@pseudo_'.$data_field['destiny'].'_destiny',
        'process' => [
          'value' => [
            'plugin' => 'format_date',
            'from_format' => 'Y-m-d H:i:s',
            'to_format' => $data_field['format'],
            'source' => 'value',
          ],
          'end_value' => [
            'plugin' => 'format_date',
            'from_format' => 'Y-m-d H:i:s',
            'to_format' => $data_field['format'],
            'source' => 'value2',
          ],
        ],
      ];
      $wildcard_yml_config['process'] = array_merge($wildcard_yml_config['process'], $array_config);
    }else{
      $wildcard_yml_config['process'] = parent::InsertBeforeKey($wildcard_yml_config['process'], 'pseudo_' . $data_field['destiny'] . '_destiny', 'pseudo_' . $origin_field, $array_config['pseudo_'.$origin_field][0]);
      $wildcard_yml_config['process']['pseudo_' . $data_field['destiny'] . '_destiny'][0]['source'][] = '@pseudo_' . $origin_field;
    }
  }
}

