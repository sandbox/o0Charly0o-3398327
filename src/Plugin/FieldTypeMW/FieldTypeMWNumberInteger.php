<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'number_integer' field type.
 *
 * @FieldType(
 *     id="number_integer",
 * )
 */
class FieldTypeMWNumberInteger extends FieldTypeMWGenericText {

}
