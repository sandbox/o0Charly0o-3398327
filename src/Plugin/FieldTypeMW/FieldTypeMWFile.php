<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'file' field type.
 *
 * @FieldType(
 *     id="file",
 * )
 */
class FieldTypeMWFile extends FieldTypeMWGenericFile {

}
