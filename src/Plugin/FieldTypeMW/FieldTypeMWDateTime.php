<?php

namespace Drupal\migrate_wizard\Plugin\FieldTypeMW;

/**
 * Provides 'datetime' field type.
 *
 * @FieldType(
 *     id="datetime",
 * )
 */
class FieldTypeMWDateTime extends FieldTypeMWGenericDate {

}
