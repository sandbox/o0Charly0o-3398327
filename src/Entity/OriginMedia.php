<?php

namespace Drupal\migrate_wizard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Migration Group entity.
 *
 * The migration group entity is used to group active migrations, as well as to
 * store shared migration configuration.
 *
 * @ConfigEntityType(
 *     id="origin_media",
 *     label=@Translation("Origin Media"),
 *     handlers={
 *         "list_builder":
 *   "Drupal\migrate_wizard\Entity\OriginMediaListBuilder",
 *     },
 *     entity_keys={
 *         "id": "id",
 *         "label": "label"
 *     },
 *     config_export={
 *         "id",
 *         "id_origin",
 *         "database",
 *         "use_env",
 *         "key",
 *         "target",
 *         "label",
 *         "description",
 *         "filemime",
 *         "source_type",
 *         "module",
 *         "shared_configuration",
 *     }
 * )
 */
class OriginMedia extends ConfigEntityBase {

}
