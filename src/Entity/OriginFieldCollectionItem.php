<?php

namespace Drupal\migrate_wizard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Migration Group entity.
 *
 * The migration group entity is used to group active migrations, as well as to
 * store shared migration configuration.
 *
 * @ConfigEntityType(
 *     id="origin_field_collection_item",
 *     label=@Translation("Origin Field Collection"),
 *     handlers={
 *         "list_builder":
 *   "Drupal\migrate_wizard\Entity\OriginFieldCollectionItemListBuilder",
 *     },
 *     entity_keys={
 *         "id": "id",
 *         "label": "label"
 *     },
 *     config_export={
 *         "id",
 *         "id_origin",
 *         "database",
 *         "use_env",
 *         "key",
 *         "target",
 *         "label",
 *         "source_type",
 *         "module",
 *         "shared_configuration",
 *     }
 * )
 */
class OriginFieldCollectionItem extends ConfigEntityBase {

}
