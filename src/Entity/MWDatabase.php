<?php

namespace Drupal\migrate_wizard\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Migration Group entity.
 *
 * The migration group entity is used to group active migrations, as well as to
 * store shared migration configuration.
 *
 * @ConfigEntityType(
 *     id="mw_database",
 *     label=@Translation("Database"),
 *     handlers={
 *         "list_builder":
 *   "Drupal\migrate_wizard\Entity\MWDatabaseListBuilder",
 *     },
 *     entity_keys={
 *         "id": "id",
 *         "label": "label"
 *     },
 *     config_export={
 *         "id",
 *         "key",
 *         "migration_group",
 *         "target",
 *         "host",
 *         "database",
 *         "username",
 *         "password",
 *         "prefix",
 *         "driver",
 *         "port",
 *         "namespace",
 *         "collation",
 *         "url_files_prod",
 *         "use_env",
 *         "shared_configuration"
 *     }
 * )
 */
class MWDatabase extends ConfigEntityBase {

}
