<?php

declare(strict_types = 1);

namespace Drupal\migrate_wizard\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\migrate_tools\Drush\Commands\MigrateToolsCommands;
use Drush\Attributes as CLI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Migrate Tools drush commands.
 */
class MWDrushToolsCommands extends MigrateToolsCommands {

  /**
   * The current mw_databases.
   *
   * @var \Drupal\migrate_wizard\Entity\MWDatabase
   */
  protected $mwDatabases;

  /**
   * {@inheritdoc}
   */
  public function __construct(MigrationPluginManager $migrationPluginManager,
                              DateFormatter $dateFormatter,
                              EntityTypeManagerInterface $entityTypeManager,
                              KeyValueFactoryInterface $keyValue) {
    parent::__construct($migrationPluginManager, $dateFormatter, $entityTypeManager, $keyValue);

    $this->mwDatabases = $this->entityTypeManager->getStorage('mw_database')->loadMultiple();

    foreach ($this->mwDatabases as $mw_database) {
      $info = [
        'host' => $mw_database->get('use_env') ? getenv($mw_database->get('host')) : $mw_database->get('host'),
        'database' => $mw_database->get('use_env') ? getenv($mw_database->get('database')) : $mw_database->get('database'),
        'username' => $mw_database->get('use_env') ? getenv($mw_database->get('username')) : $mw_database->get('username'),
        'password' => $mw_database->get('use_env') ? getenv($mw_database->get('password')) : $mw_database->get('password'),
        'prefix' => $mw_database->get('prefix'),
        'driver' => $mw_database->get('driver'),
        'port' => $mw_database->get('port'),
        'namespace' => $mw_database->get('namespace'),
        'collation' => $mw_database->get('collation'),
      ];
      Database::addConnectionInfo($mw_database->get('key'), $mw_database->get('target'), $info);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('plugin.manager.migration'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[CLI\Command(name: 'migrate:status', aliases: ['ms', 'migrate-status'])]
  #[CLI\Argument(name: 'migration_names', description: 'Restrict to a comma-separated list of migrations. Optional.')]
  #[CLI\Option(name: 'group', description: 'A comma-separated list of migration groups to import')]
  #[CLI\Option(name: 'tag', description: 'Name of the migration tag to list')]
  #[CLI\Option(name: 'names-only', description: 'Only return names, not all the details (faster)')]
  #[CLI\Option(name: 'continue-on-failure', description: 'When a migration fails, continue processing remaining migrations.')]
  #[CLI\Usage(name: 'migrate:status', description: 'Retrieve status for all migrations')]
  #[CLI\Usage(name: 'migrate:status --group=beer', description: 'Retrieve status for all migrations, grouped by tag')]
  #[CLI\Usage(name: 'migrate:status --tag=user', description: 'Retrieve status for all migrations tagged with user.')]
  #[CLI\Usage(name: 'migrate:status beer_term,beer_node', description: 'Retrieve status for specific migrations')]
  #[CLI\Usage(name: 'migrate:status --field=id', description: 'Retrieve a raw list of migration IDs.')]
  #[CLI\Usage(name: 'ms --fields=id,status --format=json', description: 'Retrieve a JSON serialized list of migrations, each item containing only the migration ID and its status.')]
  #[CLI\Topics(topics: ['migrate'])]
  #[CLI\ValidateModulesEnabled(modules: ['migrate_tools'])]
  #[CLI\FieldLabels(labels: [
    'group' => 'Group',
    'id' => 'Migration ID',
    'status' => 'Status',
    'total' => 'Total',
    'imported' => 'Imported',
    'unprocessed' => 'Unprocessed',
    'message_count' => 'Message Count',
    'last_imported' => 'Last Imported',
  ])]
  #[CLI\DefaultFields(fields: [
    'group',
    'id',
    'status',
    'total',
    'imported',
    'unprocessed',
    'message_count',
    'last_imported',
  ])]
  #[CLI\FilterDefaultField(field: 'status')]
  public function status($migration_names = '', array $options = [
    'group' => self::REQ,
    'tag' => self::REQ,
    'names-only' => FALSE,
    'continue-on-failure' => FALSE,
  ]): RowsOfFields {
    $table = [];
    foreach ($this->mwDatabases as $mw_database) {
      $options['group'][] = $mw_database->get('migration_group');
    }
    $options['group'] = implode(',', $options['group']);
    $table = parent::status($migration_names, $options);

    return $table;
  }

}
