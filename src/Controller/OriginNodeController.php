<?php

namespace Drupal\migrate_wizard\Controller;

use Drupal\migrate_wizard\MWManageDataService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller to interact with origin node entities.
 */
class OriginNodeController extends MwControllerBase {

  /**
   * The MWManageDataService.
   *
   * @var \Drupal\migrate_wizard\MWManageDataService
   */
  protected $mwManageDataService;

  /**
   * Constructs a OriginNodeController object.
   *
   * @param \Drupal\migrate_wizard\MWManageDataService $mw_manage_data_service
   *   The MWManageDataService.
   */
  public function __construct(MWManageDataService $mw_manage_data_service) {
    $this->mwManageDataService = $mw_manage_data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('migrate_wizard.migrate_wizard_manage_data_service'),
    );
  }

  /**
   * Call MWManageDataService for clear Origin Nodes.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the Origin Node list.
   */
  public function clearOriginNodes() {
    $mwDatabase = $this->mwManageDataService->clearOriginNodes();
    $this->messenger()->addStatus($this->t('Completed clear Origin Nodes.'));

    return $this->redirect('entity.origin_node.list', ['mw_database' => $mwDatabase->id]);
  }

  /**
   * Call MWManageDataService for read Origin Nodes.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the Origin Node list.
   */
  public function readOriginNodes() {
    $mwDatabase = $this->mwManageDataService->readOriginNodes();
    $this->messenger()->addStatus($this->t('Completed discovery for Origin Nodes.'));

    return $this->redirect('entity.origin_node.list', ['mw_database' => $mwDatabase->id]);
  }

}
