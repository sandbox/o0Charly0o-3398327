<?php

namespace Drupal\migrate_wizard\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a platform item annotation object.
 *
 * @Annotation
 */
class FieldType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
