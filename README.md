# Migrate Wizard

## Introduction
Migrate Wizard provides an easy-to-use solution to migrate content from Drupal 7 to newer versions without requiring programming.

## Features
- Generate connections to the source database without editing the `settings.php` file.
- Add more than one source database.
- Migrate from Content Type to Vocabulary, Paragraphs, and vice versa.
- Migrate two content types in the source to the same destination. Unify content, vocabularies, etc.
- Migrate only the files you are going to use. If needed, you can migrate them independently.
- Migrate fields or entities from file or image type to media type.
- Migrate path aliases.
- Migrate content with user ownership.
- Migrate a specific number of nodes, specific `nid`s, or nodes from a particular date.
- Migrate Field collections to paragraphs, including nested paragraphs. Start with the last child and end with the parent or grandparent.
- Migrate Drupal 7 translations made with the Content Translation (core) module (Entity translation is in progress).

## Post-installation
1. After installation, navigate to `migrate-wizard/list-databases/create-mw-database` and add a connection configuration for a Drupal 7 source database. Use the same connection information that was used in the `settings.php` file.
2. If you want to use environment variables to hide sensitive information such as usernames or passwords, you can use them in the host, database, username, and password fields.
3. Once your database is configured, go to the role mappings section. If a role does not exist in your target Drupal site, you can create it on the fly.

## Configuring Migration
- Go to the Users tab, check the **Build migration config** option, and map the user fields if necessary.
- For the rest of the migrations, the process is similar, with a few specific cases detailed below:
  - In the **Mapping formats editors** tab, select the correspondence between the listed source editors and the destination editors in the selectors.
  - In the **Node list** tab, click **Read origin nodes** to display a row for each content type from the Drupal 7 source.
  - Click **Manage fields origin nodes** for the content type you wish to migrate, where you'll configure the field mapping for migration.

### Special Cases
- **Taxonomy or relationships fields**: Select which migration corresponds to the relationship, ensuring the correct lookup of the ID for that migration.
- **File or image fields**: If the destination is file or image type (without media), the indented selectors should remain untouched. For a media type destination, select the media type and the field of the media entity that will store the data.

## Similar Projects
While there are other migration modules available, Migrate Wizard stands out for its easy-to-use interface, field mapping capabilities, and dedicated support for migrations from Drupal 7 to Drupal 10.

